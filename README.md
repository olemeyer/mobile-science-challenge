### How To Do ###

Am Anfang **einmalig** durchzuführen:

* Das Repository von Bitbucket clonen: https://youtu.be/k83MhcEG86g Die zu verwendende URL sollte auf dieser Seite oben rechts angezeigt werden. In der Regel lautet sie: 
```
#!java

https://<Nutzername>@bitbucket.org/olemeyer/mobile-science-challenge.git
```
Danach in **jedes mal** durchzuführen:

* Ein Pull-Request durchführen um den aktuelen Stand **vor** der Bearbeitung zu laden: https://youtu.be/FGkUrEaB5mM
* Einen Commit durchführen **nach** abschluss der Bearbeitung: https://youtu.be/yR4Jm0UyrXg

### Navigation UI-Elemente ###

* Splash Screen
* Einstellungen 
     * Themenbausteinfreischaltung
     * Themenbausteine
     * Kontoverwaltung (Einstieg)
     * Avatargestaltung
* Spielinformation
     * Spielübersicht (Einstieg)
     * Spielerstatistik
* Spiel
     * Neues Spiel (Einstieg)
     * Spielersuche
* Aktuelles Spiel
     * Aktives Spiel (Einstieg)
     * Einzelstatistik
* Fragerunde