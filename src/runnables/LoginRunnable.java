package runnables;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.Handler;
import android.os.Message;

import com.google.gson.Gson;

import de.paluno.se.mse.msc.gson.NutzerPaket;
import de.paluno.se.mse.msc.gson.RegistrierungAntwort;

public class LoginRunnable implements Runnable{
	NutzerPaket paket;
	private Handler handler;
	
	public LoginRunnable(NutzerPaket paket,Handler handler) {
		this.paket=paket;
		this.handler=handler;
	}

	public void run() {
		try{

			URL url=new URL("http://132.252.58.179:8080/Quizduell/");
			HttpURLConnection conn=(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setReadTimeout(10000);
			conn.setConnectTimeout(10000);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.connect();

			OutputStreamWriter out=new OutputStreamWriter(conn.getOutputStream());
			
			out.write("data="+(new Gson().toJson(paket)));
			out.flush();
			out.close();
			conn.connect();
			
			BufferedReader in=new BufferedReader(new InputStreamReader(conn.getInputStream()));
			RegistrierungAntwort answer=new Gson().fromJson(in.readLine(), RegistrierungAntwort.class);
			if(answer.getErrorcode()==0){
				Message m=new Message();
				m.getData().putInt("errorcode", answer.getErrorcode());
				m.getData().putInt("userid", answer.getPayload().getUserid());
				handler.sendMessage(m);
				System.out.println("PushID wurde zum Backend gesendet");
			}else{
				Message m=new Message();
				m.getData().putInt("errorcode", answer.getErrorcode());
				handler.sendMessage(m);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	};

}
