package runnables;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.Handler;
import android.os.Message;

import com.google.gson.Gson;

import de.paluno.se.mse.msc.gson.Jsonable;
import de.paluno.se.mse.msc.gson.RegistrierungAntwort;

public class APIRunnable implements Runnable {
	private Jsonable packet;
	private Handler answerHandler;
	
	
	
	public APIRunnable(Jsonable packet, Handler answerHandler) {
		super();
		this.packet = packet;
		this.answerHandler = answerHandler;
	}



	@Override
	public void run() {
		try{

			URL url=new URL("http://132.252.58.179:8080/Quizduell/");
			HttpURLConnection conn=(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setReadTimeout(10000);
			conn.setConnectTimeout(10000);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.connect();

			OutputStreamWriter out=new OutputStreamWriter(conn.getOutputStream());
			
			out.write("data="+(new Gson().toJson(packet)));
			out.flush();
			out.close();
			conn.connect();
			
			BufferedReader in=new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String answer=in.readLine();
			in.close();
			Message msg=new Message();
			msg.getData().putString("answer",answer );
			answerHandler.sendMessage(msg);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}

}
