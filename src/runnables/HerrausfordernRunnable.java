package runnables;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.widget.Toast;

import com.google.gson.Gson;

import de.paluno.se.mse.msc.App;
import de.paluno.se.mse.msc.NeuesSpielActivity;
import de.paluno.se.mse.msc.SplashActivity;
import de.paluno.se.mse.msc.gson.GameOverviewAnswer;
import de.paluno.se.mse.msc.gson.GameOverviewRequest;
import de.paluno.se.mse.msc.gson.RegistrierungAntwort;
import de.paluno.se.mse.msc.gson.SpielerHerausfordern;

public class HerrausfordernRunnable implements Runnable {

	private App app;
	private int uid ;
	private Handler handler;
	private SpielerHerausfordern request;
	private GameOverviewRequest gamerequest;
	
	public  HerrausfordernRunnable(App app,Handler handler , int uid) {
		this.app = app;
		this.uid = uid;
		this.handler = handler;
		
		request = new SpielerHerausfordern(new SpielerHerausfordern.Payload(uid), 5, app.getUser().getUserid(), app.getUser().getMd5pass());
		gamerequest=new GameOverviewRequest(app);
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			
			//Spieler herausfordern

			URL url=new URL("http://132.252.58.179:8080/Quizduell/");
			HttpURLConnection conn=(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setReadTimeout(10000);
			conn.setConnectTimeout(10000);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.connect();

			OutputStreamWriter out=new OutputStreamWriter(conn.getOutputStream());
			
			out.write("data="+(new Gson().toJson(request)));
			out.flush();
			out.close();
			
			conn.connect();
			BufferedReader b2r=new BufferedReader(new InputStreamReader(conn.getInputStream()));
			System.out.println("Antwort herausfordern: "+b2r.readLine());
			Message m=new Message();
			m.getData().putString("gesendet", "Anfrage versendet!");
			
			
			//Spielübersicht abfragen
			url=new URL("http://132.252.58.179:8080/Quizduell/");
			conn=(HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setReadTimeout(10000);
			conn.setConnectTimeout(10000);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.connect();

			out=new OutputStreamWriter(conn.getOutputStream());
			out.write("data="+new Gson().toJson(gamerequest));
			out.flush();
			BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
			GameOverviewAnswer answer=new Gson().fromJson(br.readLine(), GameOverviewAnswer.class);
			for(GameOverviewAnswer.Payload.Game game:answer.getPayload().getGames()){
				System.out.println(game.toString());
			}
			System.out.println("Anfrage Übersicht erfolgreich");
			
			out.close();
			
			
			handler.sendMessage(m);
			System.out.println("spielanfrage gesendet mit der uid: "+ uid);

			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
