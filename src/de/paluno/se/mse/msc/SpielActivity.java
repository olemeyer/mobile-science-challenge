package de.paluno.se.mse.msc;

import java.util.LinkedList;
import java.util.List;

import de.paluno.se.mse.msc.fragments.AktivesSpielFragment;
import de.paluno.se.mse.msc.fragments.EinzelstatistikFragment;
import de.paluno.se.mse.msc.fragments.SpielerstatistikFragment;
import de.paluno.se.mse.msc.fragments.SpieluebersichtFragment;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

public class SpielActivity extends Activity {
	
	private ViewPager viewPager;
	private final List<Fragment> fragmentList=new LinkedList<Fragment>();
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_spiel);
	viewPager = (ViewPager) findViewById(R.id.viewPager_game);
	
	fragmentList.add(new SpieluebersichtFragment());
	fragmentList.add(new SpielerstatistikFragment());
	
	 viewPager.setAdapter(new FragmentPagerAdapter(getFragmentManager()) {
			
			@Override
			public int getCount() {
				return fragmentList.size();
			}
			
			@Override
			public Fragment getItem(int pos) {
				if(pos<fragmentList.size()){
					return fragmentList.get(pos);
				}
				return null;
			}
		});
	 viewPager.setCurrentItem(0);
	
}
}
