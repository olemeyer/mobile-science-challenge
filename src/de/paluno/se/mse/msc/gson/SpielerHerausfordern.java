package de.paluno.se.mse.msc.gson;

import de.paluno.se.mse.msc.gson.NutzerPaket.Payload;

public class SpielerHerausfordern implements Jsonable{

	private int opcode=1;
	private int userid=-1;
	private String md5hash="";
	private Payload payload;
	
	
	public SpielerHerausfordern(Payload payload, int opcode, int userid, String md5hash) {
		// TODO Auto-generated constructor stub
		super();
		this.payload = payload;
		this.opcode = opcode;
		this.userid = userid;
		this.md5hash = md5hash;
	}



	public static class Payload{
		private int opponent;
		
		public Payload(int opponent) {
			super();
			this.opponent = opponent;
			
		}
		
	}
	
}
