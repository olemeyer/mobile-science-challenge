package de.paluno.se.mse.msc.gson;

import java.util.ArrayList;
import java.util.LinkedList;

public class GameOverviewAnswer implements Jsonable {
	private int errorcode;
	private int opcode;
	private Payload payload;
	
	
	
	
	public int getErrorcode() {
		return errorcode;
	}




	public void setErrorcode(int errorcode) {
		this.errorcode = errorcode;
	}




	public int getOpcode() {
		return opcode;
	}




	public void setOpcode(int opcode) {
		this.opcode = opcode;
	}




	public Payload getPayload() {
		return payload;
	}




	public void setPayload(Payload payload) {
		this.payload = payload;
	}




	public static class Payload{
		private ArrayList<Game> games;
		
		public static class Game{
			private long created;
			private int gameid;
			private long lastaction;
			private int opponentavatar;
			private int opponentid;
			private String opponentname;
			private int status;
			public long getCreated() {
				return created;
			}
			public int getGameid() {
				return gameid;
			}
			public long getLastaction() {
				return lastaction;
			}
			public int getOpponentavatar() {
				return opponentavatar;
			}
			public int getOpponentid() {
				return opponentid;
			}
			public String getOpponentname() {
				return opponentname;
			}
			public int getStatus() {
				return status;
			}
			@Override
			public String toString() {
				return "Game [created=" + created + ", gameid=" + gameid
						+ ", lastaction=" + lastaction + ", opponentavatar="
						+ opponentavatar + ", opponentid=" + opponentid
						+ ", opponentname=" + opponentname + ", status="
						+ status + "]";
			}
			
			
			
		}

		public ArrayList<Game> getGames() {
			return games;
		}

		public void setGames(ArrayList<Game> games) {
			this.games = games;
		}
	}
}
