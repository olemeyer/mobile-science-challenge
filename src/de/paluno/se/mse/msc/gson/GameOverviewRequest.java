package de.paluno.se.mse.msc.gson;

import de.paluno.se.mse.msc.App;
import android.content.Context;

public class GameOverviewRequest {
	private int opcode=4;
	private int userid;
	private String md5hash;
	private Payload payload=new Payload();
	
	public GameOverviewRequest(App app) {
		userid=app.getUser().getUserid();
		md5hash=app.getUser().getMd5pass();
	}
	
	public static class Payload{}
	
	
}
