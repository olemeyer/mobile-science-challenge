package de.paluno.se.mse.msc.gson;

public class NutzerPaket implements Jsonable {
	private int opcode=1;
	private int userid=-1;
	private String md5hash="";
	private Payload payload;
	
	
	public NutzerPaket(Payload payload, int opcode, int userid, String md5hash) {
		super();
		
		this.payload = payload;
		this.opcode = opcode;
		this.userid = userid;
		this.md5hash = md5hash;
	}



	public static class Payload{
		private String username,pushid,md5hash;
		private int avatar;
		public Payload(String username, String pushid, String md5hash,
				int avatar) {
			super();
			this.username = username;
			this.pushid = pushid;
			this.md5hash = md5hash;
			this.avatar = avatar;
		}
		
	}
}
