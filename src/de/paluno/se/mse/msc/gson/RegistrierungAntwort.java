package de.paluno.se.mse.msc.gson;

public class RegistrierungAntwort {
	private Payload payload;
	private int errorcode=0,opcode=1;
	
	
	
	public int getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(int errorcode) {
		this.errorcode = errorcode;
	}

	public int getOpcode() {
		return opcode;
	}

	public void setOpcode(int opcode) {
		this.opcode = opcode;
	}

	public Payload getPayload() {
		return payload;
	}

	public void setPayload(Payload payload) {
		this.payload = payload;
	}

	public static class Payload{
		private int userid,errorcode,opcode;

		public int getUserid() {
			return userid;
		}

		public void setUserid(int userid) {
			this.userid = userid;
		}

		public int getErrorcode() {
			return errorcode;
		}

		public void setErrorcode(int errorcode) {
			this.errorcode = errorcode;
		}

		public int getOpcode() {
			return opcode;
		}

		public void setOpcode(int opcode) {
			this.opcode = opcode;
		}
		
	}
}
