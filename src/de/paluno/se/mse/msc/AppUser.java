package de.paluno.se.mse.msc;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Hex;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.SyncStateContract.Helpers;
import de.paluno.se.mse.msc.customviews.persistenz.UserDBOpenHelper;

public class AppUser {

	private String username;
	private int userid;
	private String md5pass;
	private int avatar = 0;
	private Context context;
	private UserDBOpenHelper dbhelper;
	
	
	public AppUser(Context context) throws UserNotFoundException {
		super();
		this.context=context;
		dbhelper=new UserDBOpenHelper(context);
		//Lade Nutzerdaten aus Datenbank
		loadUser();
	}

	public AppUser(String username, int userid, String md5pass, int avatar,Context context) {
		super();
		this.username = username;
		this.userid = userid;
		this.md5pass = md5pass;
		this.avatar = avatar;
		this.context=context;
		dbhelper=new UserDBOpenHelper(context);
		saveUser();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getMd5pass() {
		return md5pass;
	}
	
	//Uebergebe Plaintext und konvertiere zu MD5
	public void setPlainPass(String pass){
		try {
			byte[] md5bytes=MessageDigest.getInstance("MD5").digest(pass.getBytes());
			md5pass=new String(Hex.encodeHex(md5bytes));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setMd5pass(String md5pass) {
		this.md5pass = md5pass;
	}

	public int getAvatar() {
		return avatar;
	}

	public void setAvatar(int avatar) {
		this.avatar = avatar;
	}
	
	//Speicher Nutzer in Datenbank
	public void saveUser(){
		SQLiteDatabase db=dbhelper.getWritableDatabase();
		db.delete(dbhelper.dbname, null, null);
		ContentValues values=new ContentValues();
		values.put("username", getUsername());
		values.put("userid", getUserid());
		values.put("md5pass", getMd5pass());
		values.put("avatar", getAvatar());
		db.insert(dbhelper.dbname, null, values);
		db.close();
	}
	
	//Lade Nutzer aus Datenbank
	public void loadUser() throws UserNotFoundException{
		SQLiteDatabase db=dbhelper.getReadableDatabase();
		Cursor c=db.query(dbhelper.dbname,
				new String[]{"username","userid","md5pass","avatar"},
				null, null, null, null, null);
		if(c==null||!c.moveToFirst())throw new UserNotFoundException();
		setUsername(c.getString(0));
		setUserid(c.getInt(1));
		setMd5pass(c.getString(2));
		setAvatar(c.getInt(avatar));
		db.close();
	}
	
	public static class UserNotFoundException extends Exception{}

	@Override
	public String toString() {
		return "AppUser [username=" + username + ", userid=" + userid
				+ ", md5pass=" + md5pass + ", avatar=" + avatar + "]";
	}
	
	

}
