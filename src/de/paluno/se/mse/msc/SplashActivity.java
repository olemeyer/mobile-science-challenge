package de.paluno.se.mse.msc;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Semaphore;

import runnables.LoginRunnable;
import runnables.RegisterRunnable;

import com.google.gson.Gson;

import de.paluno.se.mse.msc.AppUser.UserNotFoundException;
import de.paluno.se.mse.msc.gcm.GcmAutoLoginRunnable;
import de.paluno.se.mse.msc.gson.NutzerPaket;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

///TEST1

public class SplashActivity extends Activity {
	private Intent startSettings;
	
	private Handler gcmautologinhandler=new Handler(new android.os.Handler.Callback(){

		@Override
		public boolean handleMessage(final Message msg) {
			if(msg.getData().getInt("errorcode")==0){
				Toast.makeText(SplashActivity.this, "Pushid wurde erfolgrteich an das Backend gesendet.", Toast.LENGTH_LONG).show();
			}else{
				Toast.makeText(SplashActivity.this, "Pushid konnte nicht an das Backend gesendet werden. Fehlercode: "+msg.getData().getInt("errorcode"), Toast.LENGTH_LONG).show();

			}
			return true;
		}
	});
	
		

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_splash);
			final App app = (App) getApplication();
			final Semaphore semaphore = new Semaphore(1);
			semaphore.acquire(1);
			// Lade Nutzer
			new Thread() {
				public void run() {
					try {
						AppUser user = new AppUser(SplashActivity.this);
						app.setUser(user);
						new Thread(new GcmAutoLoginRunnable(SplashActivity.this, app,gcmautologinhandler)).start();
					//	app.setAngemeldet(true); //nutzer als angemeldet markieren
						SplashActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(SplashActivity.this,
										"Nutzer wurde aus Speicher geladen.",
										Toast.LENGTH_SHORT).show();
							}
						});
						
					} catch (UserNotFoundException e) {
						SplashActivity.this.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(
										SplashActivity.this,
										"Es wurden keine Nutzerdaten gefunden.",
										Toast.LENGTH_LONG).show();
							}
						});
					}
					//Überprüfe ob die min Wartezeit des Splashscreens (anderer Thread!!!)
					try {
						semaphore.acquire(1);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					// Entferne Splashscreen von Backstack
					startSettings = new Intent(getApplicationContext(),
							SettingsActivity.class);
					startSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
							| Intent.FLAG_ACTIVITY_CLEAR_TASK);
					startActivity(startSettings);
				};
			}.start();

			
			// new Thread(new LoginRunnable(login)).start();

			// Starte Timer und zeige den Splashscreen für x Millisekunden
			
			new Timer().schedule(new TimerTask() {

				@Override
				public void run() {
					semaphore.release(1);
				}
			}, 3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
