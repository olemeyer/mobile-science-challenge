package de.paluno.se.mse.msc;

import android.app.Application;

public class App extends Application {
	
	//Ist null wenn keine Daten hinterlegt wurden
	private AppUser user;
	
	private String pushId = "";
	private int loginopcode = 12;//1
	private int registeropcode=1;
	private boolean angemeldet = false;
	
	
	public synchronized String getPushId(){
		return pushId;
	}
	public synchronized void setPushId(String pid){
		this.pushId=pid;
	}
	
	public synchronized AppUser getUser() {
		return user;
	}
	public synchronized void setUser(AppUser user) {
		this.user = user;
	}

	public synchronized int getLoginopcode() {
		return loginopcode;
	}

	public synchronized void setLoginopcode(int loginopcode) {
		this.loginopcode = loginopcode;
	}

	public synchronized int getRegisteropcode() {
		return registeropcode;
	}

	public synchronized void setRegisteropcode(int registeropcode) { 
		this.registeropcode = registeropcode;
	}
	public synchronized boolean isAngemeldet() {
		return angemeldet;
	}
	public synchronized void setAngemeldet(boolean angemeldet) {
		this.angemeldet = angemeldet;
	}
	
	

}
