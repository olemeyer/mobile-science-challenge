package de.paluno.se.mse.msc;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;



















import de.paluno.se.mse.msc.fragments.AvatargestaltungFragment;
import de.paluno.se.mse.msc.fragments.KontoVerwaltungFragment;
import de.paluno.se.mse.msc.fragments.NutzerAngemeldetFragment;
import de.paluno.se.mse.msc.fragments.ThemenbausteineFragment;
import de.paluno.se.mse.msc.fragments.ThemenbausteinfreischaltungFragment;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;


public class SettingsActivity extends Activity {
	
	
	
	private final static int seitenZahl= 4;
	private ViewPager viewPager;
	private final List<Fragment> fragmentList=new LinkedList<Fragment>();
	private List<ImageView>punkte;
	private App app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        viewPager=(ViewPager) findViewById(R.id.viewPager_settings);
        app = (App) getApplication();
        fragmentList.add(new ThemenbausteinfreischaltungFragment());
        fragmentList.add(new ThemenbausteineFragment());
        if(app.getUser() != null){
        	fragmentList.add(new NutzerAngemeldetFragment());
        }
        else{
        	fragmentList.add(new KontoVerwaltungFragment());
        }
        
        fragmentList.add(new AvatargestaltungFragment());
        //Erstelle Adapter der die Fragmente der Einstellungen verwaltet
        viewPager.setAdapter(new FragmentPagerAdapter(getFragmentManager()) {
			
			@Override
			public int getCount() {
				return fragmentList.size();
			}
			
			@Override
			public Fragment getItem(int pos) {
				if(pos<fragmentList.size()){
					return fragmentList.get(pos);
				}
				return null;
			}
		});
        viewPager.setCurrentItem(2);
        addDots();
        
    
    
   


}
    
    public void selectDot (int idx){
  	  Resources res= getResources();
  	  for(int i = 0; i < seitenZahl; i++){
  		  int drawableId = (i==idx)?(R.drawable.dot_selected):(R.drawable.dot_unselected);
  		  Drawable drawable = res.getDrawable(drawableId);
  		  punkte.get(i).setImageDrawable(drawable);
  	  }
    }
    
  public void addDots(){
	  punkte=new ArrayList<ImageView>();
	  LinearLayout dotsLayout = (LinearLayout)findViewById(R.id.dots);
	  for(int i = 0; i < seitenZahl; i++) {
		  ImageView dot= new ImageView(this);
		  dot.setImageDrawable(getResources().getDrawable(R.drawable.dot_unselected));
	
		  LinearLayout.LayoutParams params= new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
	                LinearLayout.LayoutParams.WRAP_CONTENT);
		  params.setMargins(3, 0, 0, 0);
		  dotsLayout.addView(dot,params);
		  punkte.add(dot);	 
	  }
	  
	  viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
		
		@Override
		public void onPageSelected(int position) {
			selectDot(position);
		}
		
		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onPageScrollStateChanged(int arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	 
	  
	  selectDot(2);
	
	  
  }

}
