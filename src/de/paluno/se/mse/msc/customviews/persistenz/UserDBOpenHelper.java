package de.paluno.se.mse.msc.customviews.persistenz;

import de.paluno.se.mse.msc.AppUser;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class UserDBOpenHelper extends SQLiteOpenHelper {

	public static final int dbversion=2;
	public static final String dbname="user";
	
	public UserDBOpenHelper(Context context) {
		super(context, dbname, null, dbversion);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table user (username TEXT, userid INTEGER, md5pass TEXT, avatar INTEGER)");

	}
	
	

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

	}

}
