package de.paluno.se.mse.msc.customviews.persistenz;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class GameDBOpenHelper extends SQLiteOpenHelper {
	
	private static String createquery="create table game (gameid long primary key, create_time long, last_time long, avatar long, gegner_uid, gegner_name text, game_status integer)";

	public GameDBOpenHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(createquery);

	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}

}
