package de.paluno.se.mse.msc.customviews;

import java.util.LinkedList;
import java.util.List;

public class NotEmptyConstraint implements Constraint {
	private boolean lastcheck=false;
	
	@Override
	public boolean check(ConstraintEditText constraintEditText) {
		lastcheck=!constraintEditText.getText().toString().trim().isEmpty();
		return lastcheck;
	}

}
