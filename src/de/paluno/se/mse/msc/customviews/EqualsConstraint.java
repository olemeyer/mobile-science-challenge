package de.paluno.se.mse.msc.customviews;

import java.util.LinkedList;
import java.util.List;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class EqualsConstraint implements Constraint {
	private EditText[] editTextArr;
	private boolean lastcheck=false;
	
	public EqualsConstraint(final EditText... editTextArr) {
		super();
		this.editTextArr = editTextArr;
		for(EditText et:editTextArr){
			et.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					for(EditText et2:editTextArr){
						et2.invalidate();
					}
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					
				}
			});
		}
	}

	@Override
	public boolean check(ConstraintEditText constraintEditText) {
		boolean checkflag=true;
		for(EditText et:editTextArr)if(!et.getText().toString().equals(constraintEditText.getText().toString()))checkflag=false;
		lastcheck=checkflag;
		return checkflag;
	}


}
