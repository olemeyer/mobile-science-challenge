package de.paluno.se.mse.msc.customviews;

public class UsernameExistsConstraint implements Constraint {

	@Override
	public boolean check(ConstraintEditText constraintEditText) {
		String userid=constraintEditText.getText().toString();
		if(userid.isEmpty()==false) return true;
		return false;
	}

}
