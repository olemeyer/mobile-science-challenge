package de.paluno.se.mse.msc.customviews;

import java.util.LinkedList;
import java.util.List;

import android.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

public class ConstraintEditText extends EditText{
	
	private List<Constraint> constraintList=new LinkedList<Constraint>();
	private List<UpdateListener> updateListeners=new LinkedList<ConstraintEditText.UpdateListener>();
	
	public ConstraintEditText(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	public ConstraintEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ConstraintEditText(Context context) {
		super(context);
	}
	
	public void addConstraint(Constraint c){
		constraintList.add(c);
		for(UpdateListener u:updateListeners)u.update();
	}
	public void removeConstraint(Constraint c){
		constraintList.remove(c);
		for(UpdateListener u:updateListeners)u.update();
	}
	
	public boolean getConstraintState(){
		boolean checkflag=true;
		for(Constraint c:constraintList)if(!c.check(this))checkflag=false;
		return checkflag;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		boolean checkflag=true;
		for(Constraint c:constraintList)if(!c.check(this))checkflag=false;
		
		Drawable d=null;
		if(checkflag){
			d=getResources().getDrawable(R.drawable.presence_online);
		}else{
			d=getResources().getDrawable(R.drawable.presence_offline);
		}
		d.setBounds((int)(getWidth()*0.85), 0, (int)(getWidth()*0.85+getHeight()*0.5), (int)(getHeight()*0.5));
		d.draw(canvas);
		for(UpdateListener u:updateListeners)u.update();
	}
	
	public void addUpdateListenet(UpdateListener updateListener){
		this.updateListeners.add(updateListener);
	}
	
	public interface UpdateListener{
		public void update();
	}
	
}
