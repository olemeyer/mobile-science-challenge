package de.paluno.se.mse.msc.customviews;

public interface Constraint {
	public boolean check(ConstraintEditText constraintEditText);
}
