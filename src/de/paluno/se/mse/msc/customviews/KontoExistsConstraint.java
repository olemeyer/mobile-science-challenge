package de.paluno.se.mse.msc.customviews;

public class KontoExistsConstraint implements Constraint {

	ConstraintEditText userid;
	
	public KontoExistsConstraint(ConstraintEditText userid){
		this.userid = userid;
	}
	@Override
	public boolean check(ConstraintEditText constraintEditText) {
		String password=constraintEditText.getText().toString();
		String userid=this.userid.getText().toString();
		if(userid.isEmpty()==false && password.isEmpty()==false) return true;
		return false;
	}

}
