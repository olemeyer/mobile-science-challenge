package de.paluno.se.mse.msc.customviews;

import java.util.Arrays;

import de.paluno.se.mse.msc.customviews.ConstraintEditText.UpdateListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

public class ConstraintWatcher {
	private ConstraintEditText[] constraintEditTexts;
	private View v;

	public ConstraintWatcher(final View v,final ConstraintEditText... constraintEditTexts) {
		super();
		this.v=v;
		this.constraintEditTexts = constraintEditTexts;
		
		for(ConstraintEditText cedt:constraintEditTexts){
			cedt.addUpdateListenet(new UpdateListener() {
				
				@Override
				public void update() {
					boolean checkflag=true;
					for(ConstraintEditText cedt:constraintEditTexts){
						if(!cedt.getConstraintState())checkflag=false;
					}
					v.setEnabled(checkflag);
					
				}
			});
		}
	};
	
}
