package de.paluno.se.mse.msc.fragments;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;

import javax.security.auth.callback.Callback;

import runnables.APIRunnable;
import runnables.LoginRunnable;
import runnables.RegisterRunnable;

import com.google.gson.Gson;

import de.paluno.se.mse.msc.App;
import de.paluno.se.mse.msc.AppUser;
import de.paluno.se.mse.msc.SpielActivity;
import de.paluno.se.mse.msc.R;
import de.paluno.se.mse.msc.SplashActivity;
import de.paluno.se.mse.msc.R.layout;
import de.paluno.se.mse.msc.customviews.Constraint;
import de.paluno.se.mse.msc.customviews.ConstraintEditText;
import de.paluno.se.mse.msc.customviews.ConstraintWatcher;
import de.paluno.se.mse.msc.customviews.EqualsConstraint;
import de.paluno.se.mse.msc.customviews.NotEmptyConstraint;
import de.paluno.se.mse.msc.customviews.KontoExistsConstraint;
import de.paluno.se.mse.msc.customviews.UsernameExistsConstraint;
import de.paluno.se.mse.msc.customviews.PasswordSicherheitConstraint;
import de.paluno.se.mse.msc.gcm.GcmAutoLoginRunnable;
import de.paluno.se.mse.msc.gson.NutzerPaket;
import de.paluno.se.mse.msc.gson.RegistrierungAntwort;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

public class KontoVerwaltungFragment extends Fragment {
	
	private Constraint notEmptyConstraint,equalsConstraint,UsernameExistsConstraint, KontoExistsConstraint, PasswordSicherheitConstraint;
	private Handler gcmautologinhandler=new Handler(new android.os.Handler.Callback(){

		@Override
		public boolean handleMessage(final Message msg) {
			if(msg.getData().getInt("errorcode")==0){
				Toast.makeText(getActivity(), "Pushid wurde erfolgrteich an das Backend gesendet.", Toast.LENGTH_LONG);
				
			}else{
				Toast.makeText(getActivity(), "Pushid konnte nicht an das Backend gesendet werden. Fehlercode: "+msg.getData().getInt("errorcode"), Toast.LENGTH_LONG);

			}
			return true;
		}
	});
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		String myUsername;
		View view = inflater.inflate(R.layout.fragment_kontoverwaltung,
				container, false);
		final Button btnKonto = (Button) view
				.findViewById(R.id.button_new_konto);
		final Button spieluebersicht = (Button) view
				.findViewById(R.id.buttonSwitchToSpieluebersicht);
		final CheckBox kontoExists = (CheckBox) view
				.findViewById(R.id.checkbox_konto_exits);
		
		final App app = (App)getActivity().getApplication();
		
		//////VORSCHLAG IMPLEMENTIERUNG EDITTEXT ALS WIEDERVERWERTBARE KOMPONENTE////
		
		final ConstraintEditText username = (ConstraintEditText) view
				.findViewById(R.id.editText_username);
		final ConstraintEditText password = (ConstraintEditText) view
				.findViewById(R.id.editText_password);
		final ConstraintEditText password2 = (ConstraintEditText) view
				.findViewById(R.id.editText_password2);
		
		//Erstelle Constraints die von der Eingabe erfuellt werden muessen
		notEmptyConstraint=new NotEmptyConstraint(); //Eingabe darf nicht leer sein
		equalsConstraint=new EqualsConstraint(password,password2); //Eingaben muessen gleich sein
		UsernameExistsConstraint=new UsernameExistsConstraint(); //Überprüft ob ein Konto zum Benutzernamen vorhanden ist
		KontoExistsConstraint = new KontoExistsConstraint(username);
		PasswordSicherheitConstraint = new PasswordSicherheitConstraint(); //Passwort muss z.B laenger als 5 Zeichen sein
		
		//Fuege die Constraints den entsprechenden Textboxen hinzu
		username.addConstraint(notEmptyConstraint);
		password.addConstraint(notEmptyConstraint);
		password.addConstraint(PasswordSicherheitConstraint);
		password.addConstraint(equalsConstraint);
		password2.addConstraint(notEmptyConstraint);
		password2.addConstraint(equalsConstraint);
		password2.addConstraint(PasswordSicherheitConstraint);
		new ConstraintWatcher(btnKonto, username,password,password2);
		
		///////////////////////////////////////////////////////////////////////////////
		
		// spieluebersicht.setEnabled(false); // Aus Testzwecken

		kontoExists.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				if (isChecked) {
					btnKonto.setText(R.string.konto_anmelden);
					password2.setVisibility(View.INVISIBLE);
					password2.removeConstraint(notEmptyConstraint);
					password2.removeConstraint(equalsConstraint);
					password.removeConstraint(equalsConstraint);
					username.addConstraint(UsernameExistsConstraint);
					password.addConstraint(KontoExistsConstraint);
					password.removeConstraint(PasswordSicherheitConstraint);
					password2.removeConstraint(PasswordSicherheitConstraint);
				} else {
					btnKonto.setText(R.string.konto_anlegen);
					password2.setVisibility(View.VISIBLE);
					password2.addConstraint(notEmptyConstraint);
					password2.addConstraint(equalsConstraint);
					password.addConstraint(PasswordSicherheitConstraint);
					password2.addConstraint(PasswordSicherheitConstraint);
					password.addConstraint(equalsConstraint);
					username.removeConstraint(UsernameExistsConstraint);
					password.removeConstraint(KontoExistsConstraint);
				}

			}
		});

		btnKonto.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				if (kontoExists.isChecked()) {
					//Versuche Nutzer anzumelden
					final AppUser user=new AppUser(username.getText().toString()
							, -1, "", 0, getActivity());
					user.setPlainPass(password.getText().toString());
					NutzerPaket request=new NutzerPaket(
							new NutzerPaket.Payload(user.getUsername(), app.getPushId(), user.getMd5pass(), user.getAvatar()), app.getLoginopcode(), user.getUserid(), user.getMd5pass());
					
					System.out.println("Sende PushID zum Backend");
					
					new Thread(new GcmAutoLoginRunnable(getActivity(), app, new APIRunnable(request, new Handler(new android.os.Handler.Callback() {
						
						@Override
						public boolean handleMessage(Message msg) {
							String answer=msg.getData().getString("answer");
							RegistrierungAntwort answer_packet=new Gson().fromJson(answer, RegistrierungAntwort.class);
							if(answer_packet.getErrorcode()==0){
								int userid=answer_packet.getPayload().getUserid();
								Toast t=Toast.makeText(getActivity(), "Login war erfolgreich. NutzerID: "+userid, Toast.LENGTH_LONG);
								t.show();
								user.setUserid(userid);
								App app=(App) KontoVerwaltungFragment.this.getActivity().getApplication();
								app.setUser(user);
								new Thread(){
									public void run() {
										user.saveUser();
										System.out.println("Nutzer wurde gespeichert");
									};
								}.start();
							}else{
								Toast t=Toast.makeText(getActivity(), "Fehler bei dem Login. Fehlercode: "+answer_packet.getErrorcode(), Toast.LENGTH_LONG);
								t.show();
							}
							return true;
						}
					})))).start();
									
				} else {
					//Versuche neuen Nutzer zu registrieren
					final AppUser user=new AppUser(username.getText().toString()
							, -1, "", 0, getActivity());
					user.setPlainPass(password.getText().toString());
					NutzerPaket request=new NutzerPaket(
							new NutzerPaket.Payload(user.getUsername(), app.getPushId(), user.getMd5pass(), user.getAvatar()), app.getRegisteropcode(), -1, user.getMd5pass());
					System.out.println(new Gson().toJson(request).toString());
					
					new Thread(new GcmAutoLoginRunnable(getActivity(), app, new APIRunnable(request, new Handler(new android.os.Handler.Callback() {
						
						@Override
						public boolean handleMessage(Message msg) {
							RegistrierungAntwort answer_packet=new Gson().fromJson(msg.getData().getString("answer"), RegistrierungAntwort.class);
							if(answer_packet.getErrorcode()==0){
								int userid=answer_packet.getPayload().getUserid();
								Toast t=Toast.makeText(getActivity(), "Registrierung war erfolgreich. NutzerID: "+userid, Toast.LENGTH_LONG);
								t.show();
								user.setUserid(userid);
								App app=(App) KontoVerwaltungFragment.this.getActivity().getApplication();
								app.setUser(user);
								new Thread(){
									public void run() {
										user.saveUser();
										System.out.println("Nutzer wurde gespeichert");
									};
								}.start();
							}else{
								Toast t=Toast.makeText(getActivity(), "Fehler bei der Registrierung. Fehlercode: "+answer_packet.getErrorcode(), Toast.LENGTH_LONG);
								t.show();
							}
							return true;
						}
					})))).start();
					
				}
			}
		});

		spieluebersicht.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), SpielActivity.class);

				startActivity(intent);

			}
		});
		return view;
	}

}
