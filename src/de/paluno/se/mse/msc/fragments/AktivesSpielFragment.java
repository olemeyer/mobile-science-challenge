package de.paluno.se.mse.msc.fragments;

import de.paluno.se.mse.msc.R;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.Button;

public class AktivesSpielFragment extends Fragment {
	
	OnFragmentInteractionListener myListener;
	
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	View view = inflater.inflate(R.layout.fragment_aktives_spiel, container, false);
	
	
	Button fragerunde = (Button) view.findViewById(R.id.buttonAktivesSpielSwitschFragerunde);
	fragerunde.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(myListener != null) myListener.onTransitionToFragerunde();
			
		}
	});
	
	return view;
}

@Override
public void onAttach(Activity activity) {
	// TODO Auto-generated method stub
	super.onAttach(activity);
	try {
		myListener = (OnFragmentInteractionListener) activity;
	} catch (Exception e) {
		// TODO: handle exception
	}
}

public interface OnFragmentInteractionListener {
	public void onTransitionToFragerunde();


}

}
