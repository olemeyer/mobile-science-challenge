package de.paluno.se.mse.msc.fragments;

import de.paluno.se.mse.msc.App;
import de.paluno.se.mse.msc.R;
import de.paluno.se.mse.msc.SpielActivity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class NutzerAngemeldetFragment extends Fragment {
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_nutzer_angemeldet, container, false);
		App app = (App) getActivity().getApplication();
		
		TextView tv = (TextView) view.findViewById(R.id.textViewHelloNutzerAngemeldet);
		Button spieluebersicht = (Button) view.findViewById(R.id.buttonSwitchToSpieluebersicht2);
		tv.setText("Hallo "+ app.getUser().getUsername());
		spieluebersicht.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(getActivity(), SpielActivity.class);

				startActivity(intent);

			}
		});
		
		
		
		return view;
	}

}
