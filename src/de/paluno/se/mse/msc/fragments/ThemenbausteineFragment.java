package de.paluno.se.mse.msc.fragments;

import de.paluno.se.mse.msc.R;
import de.paluno.se.mse.msc.R.layout;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ThemenbausteineFragment extends Fragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_themenbausteine,
				container, false);
		return view;
	}
}
