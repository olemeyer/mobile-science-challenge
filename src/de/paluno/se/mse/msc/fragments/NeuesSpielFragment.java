package de.paluno.se.mse.msc.fragments;

import runnables.HerrausfordernRunnable;
import de.paluno.se.mse.msc.App;
import de.paluno.se.mse.msc.R;
import de.paluno.se.mse.msc.SplashActivity;
import de.paluno.se.mse.msc.fragments.AktivesSpielFragment.OnFragmentInteractionListener;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class NeuesSpielFragment extends Fragment {
	
OnFragmentTransitionToSpielersuche myListener;
App app;


private Handler herrausforderungsHandler=new Handler(new android.os.Handler.Callback(){

	@Override
	public boolean handleMessage(final Message msg) {
		Toast.makeText(getActivity(), msg.getData().getString("gesendet"), Toast.LENGTH_LONG).show();
		return true;
	}
});

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	View view = inflater.inflate(R.layout.fragment_neues_spiel, container, false);

	app = (App) getActivity().getApplication();
	
	Button spielersuche = (Button) view.findViewById(R.id.buttonNeuesSpielSwitchSpielersuche);
	spielersuche.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			if(myListener != null) myListener.onTransitionToSpielersuche();

			
		}
	});
	
	Button randomSpieler = (Button) view.findViewById(R.id.buttonBelibigerSpieler);
	randomSpieler.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			new Thread(new HerrausfordernRunnable(app, herrausforderungsHandler, -1)).start();
			
		}
	});
	
	
	return view;
}

public interface OnFragmentTransitionToSpielersuche {
	public void onTransitionToSpielersuche();
}


@Override
public void onAttach(Activity activity) {
	// TODO Auto-generated method stub
	super.onAttach(activity);
	try {
		myListener = (OnFragmentTransitionToSpielersuche) activity;
	} catch (Exception e) {
		// TODO: handle exception
	}
}


}
