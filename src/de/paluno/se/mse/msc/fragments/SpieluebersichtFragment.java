package de.paluno.se.mse.msc.fragments;


import de.paluno.se.mse.msc.AktuellesSpielActivity;


import de.paluno.se.mse.msc.NeuesSpielActivity;
import de.paluno.se.mse.msc.R;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ListView;

public class SpieluebersichtFragment extends Fragment {
	
	Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			
		};
	};
	
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
	View view = inflater.inflate(R.layout.fragment_spieluebersicht, container, false);
	
	ListView liste = (ListView) view.findViewById(R.id.listViewSpielUebersicht);
	
	
	
	view.findViewById(R.id.buttonSwitchToAktuellesspiel).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(getActivity(), AktuellesSpielActivity.class);
			
			startActivity(intent);
		}
	});
	
	view.findViewById(R.id.buttonSwitchToNeuesSpiel).setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(getActivity(), NeuesSpielActivity.class);
			
			startActivity(intent);
		}
	});
	
	return view;
}
}
