package de.paluno.se.mse.msc.gcm;

import java.io.IOException;

import runnables.LoginRunnable;
import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.gson.Gson;

import de.paluno.se.mse.msc.App;
import de.paluno.se.mse.msc.AppUser;
import de.paluno.se.mse.msc.gson.NutzerPaket;
import de.paluno.se.mse.msc.gson.NutzerPaket.Payload;

public class GcmAutoLoginRunnable implements Runnable {

	private Activity activity;
	
	private App app;
	private Handler handler;
	//Runnable die nach dem Empfang der PushID ausgeführt werden soll. Falls null wird Login durchgeführt
	private Runnable runnable;
	
	public GcmAutoLoginRunnable(Activity activity, App app,Handler handler) {
		super();
		this.activity = activity;
		this.app = app;
		this.handler=handler;
	}
	

	public GcmAutoLoginRunnable(Activity activity, App app,
			Runnable runnable) {
		super();
		this.activity = activity;
		this.app = app;
		this.runnable = runnable;
	}


	@Override
	public void run() {
		if(true){
			//InstanceID instanceID=InstanceID.getInstance(activity.getApplicationContext());
			try {
				//String pushid=instanceID.getToken("741926386738", GoogleCloudMessaging.INSTANCE_ID_SCOPE);
				final String pushid=GoogleCloudMessaging.getInstance(activity.getApplicationContext()).register("741926386738");
				System.out.println("PushID: "+pushid);
				app.setPushId(pushid);
				activity.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						Toast.makeText(activity, "PushID: "+pushid, Toast.LENGTH_LONG).show();
					}
				});
				if(runnable!=null){
					runnable.run();
				}else{
					login();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
		if (resultCode != ConnectionResult.SUCCESS) {
		if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
			GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
					9000).show();
		} else {
			Log.i(activity.getPackageName(), "This device is not supported.");
			activity.finish();
		}
			return false;
		}
	return true;
	}
	
	private void login(){
		AppUser user=app.getUser();
		NutzerPaket request=new NutzerPaket(
				new NutzerPaket.Payload(user.getUsername(), app.getPushId(), user.getMd5pass(), user.getAvatar()), app.getLoginopcode(), user.getUserid(), user.getMd5pass());
		
		System.out.println("Sende PushID zum Backend");
		new Thread(new LoginRunnable(request,handler)).start();
	}

}
