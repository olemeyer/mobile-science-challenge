package de.paluno.se.mse.msc.gcm;

import java.util.Random;

import de.paluno.se.mse.msc.R;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class GcmBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context arg0, Intent arg1) {
		String msg=arg1.getStringExtra("data");
		Notification noti=new Notification.Builder(arg0).setContentInfo("Mobile Science Challenge")
			.setContentText(msg)
			.setSmallIcon(R.drawable.science_icon).build();
		NotificationManager nm=(NotificationManager) arg0.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.notify(new Random().nextInt(), noti);
		System.out.println("Pushnachricht: "+msg);
	}

}
