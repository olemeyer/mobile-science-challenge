package de.paluno.se.mse.msc;

import java.util.LinkedList;
import java.util.List;

import de.paluno.se.mse.msc.fragments.AktivesSpielFragment;
import de.paluno.se.mse.msc.fragments.FragerundeFragment;
import de.paluno.se.mse.msc.fragments.SpielersucheFragment;
import de.paluno.se.mse.msc.fragments.AktivesSpielFragment.OnFragmentInteractionListener;
import de.paluno.se.mse.msc.fragments.EinzelstatistikFragment;
import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

public class AktuellesSpielActivity extends Activity implements OnFragmentInteractionListener{
	
	private ViewPager viewPager;
	private final List<Fragment> fragmentList=new LinkedList<Fragment>();
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_aktuelles_spiel);
	viewPager = (ViewPager) findViewById(R.id.viewPager_aktuelles_spiel);
	fragmentList.add(new AktivesSpielFragment());
	fragmentList.add(new EinzelstatistikFragment());
	
	
	 viewPager.setAdapter(new FragmentPagerAdapter(getFragmentManager()) {
			
			@Override
			public int getCount() {
				return fragmentList.size();
			}
			
			@Override
			public Fragment getItem(int pos) {
				if(pos<fragmentList.size()){
					return fragmentList.get(pos);
				}
				return null;
			}
		});
	 viewPager.setCurrentItem(0);
}

@Override
public void onTransitionToFragerunde() {
	// TODO Auto-generated method stub
	
//	getFragmentManager().beginTransaction().replace(R.id.aktuellesSpiel_container, new FragerundeFragment()).commit();
	Intent intent = new Intent(this, FragerundeActivity.class);
	
	startActivity(intent);
	
}




}
