package de.paluno.se.mse.msc;


import de.paluno.se.mse.msc.fragments.FragerundeFragment;
import de.paluno.se.mse.msc.fragments.NeuesSpielFragment;
import de.paluno.se.mse.msc.fragments.AktivesSpielFragment.OnFragmentInteractionListener;
import de.paluno.se.mse.msc.fragments.NeuesSpielFragment.OnFragmentTransitionToSpielersuche;
import de.paluno.se.mse.msc.fragments.SpielersucheFragment;
import android.app.Activity;
import android.os.Bundle;

public class NeuesSpielActivity extends Activity implements OnFragmentTransitionToSpielersuche  {
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	if (savedInstanceState == null) {
		getFragmentManager().beginTransaction().add(R.id.neuesSpiel_container, new NeuesSpielFragment()).commit();
		
//				.add(R.id.container, new SplashScreenFragment()).commit();
	}
	setContentView(R.layout.activity_neues_spiel);
}

@Override
public void onTransitionToSpielersuche() {
	// TODO Auto-generated method stub
	
	getFragmentManager().beginTransaction().replace(R.id.neuesSpiel_container, new SpielersucheFragment()).addToBackStack(null).commit();
	
}




}
